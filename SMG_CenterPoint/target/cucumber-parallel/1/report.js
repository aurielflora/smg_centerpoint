$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/auriel.flora/workspace_smg/SMG_CenterPoint/src/test/resources/features/searchservicerequest.feature");
formatter.feature({
  "line": 2,
  "name": "Search Service Request",
  "description": "",
  "id": "search-service-request",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@searchservicerequest"
    },
    {
      "line": 1,
      "name": "@scenarios"
    }
  ]
});
formatter.before({
  "duration": 4607710125,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Search is activated when hovering over Search icon",
  "description": "",
  "id": "search-service-request;search-is-activated-when-hovering-over-search-icon",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@smg_cp_test"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I am logged in as \u0027Requester\u0027",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "Requester",
      "offset": 19
    }
  ],
  "location": "loginStepdefs.java:13"
});
formatter.result({
  "duration": 19520989653,
  "status": "passed"
});
formatter.after({
  "duration": 669865922,
  "status": "passed"
});
});