import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"C:/Users/auriel.flora/workspace_smg/SMG_CenterPoint/src/test/resources/features/searchservicerequest.feature"},
        plugin = {"json:C:/Users/auriel.flora/workspace_smg/SMG_CenterPoint/target/cucumber-parallel/1.json", "html:C:/Users/auriel.flora/workspace_smg/SMG_CenterPoint/target/cucumber-parallel/1"},
        monochrome = false,
        tags = {"@smg_cp_test"},
        glue = {"com.smg.cp.stepdefs"})
public class Parallel01IT {
}
