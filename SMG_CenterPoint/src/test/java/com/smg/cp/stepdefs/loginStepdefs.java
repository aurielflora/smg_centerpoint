package com.smg.cp.stepdefs;

import org.hamcrest.Matchers;
import org.junit.Assert;

import com.smg.cp.pageobjects.LoginPage;

import cucumber.api.java8.En;

public class loginStepdefs implements En{
	
	public loginStepdefs(ScenarioHooks hooks, LoginPage loginPage) {
		Given("^I am logged in as '(.*)'$", (String userRole) -> {
			loginPage.setDriver(hooks.getDriverNavigator(), hooks.getScenarioName());
			Assert.assertThat(loginPage.loginCenterPoint(userRole), Matchers.equalTo(true));
		});
	}
}
