package com.smg.cp.pageobjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.cucumber.listener.Reporter;
import com.smg.cp.utils.PropertyUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginPage extends BasePage {
	private static final Logger log = LogManager.getLogger(LoginPage.class);
	private String username;
	private String password;

	@FindBy(css = "div[class='login-username-box']")
	private WebElement fld_Username;

	@FindBy(css = "div[class='login-password-box']")
	private WebElement fld_Password;

	@FindBy(css = "div[class='submit']")
	private WebElement btn_LoginSubmit;

	HashMap<String, HashMap<String,String>> userType = new HashMap<String,HashMap<String,String>>();
    HashMap<String,String> userCredentials =  new HashMap<String,String>();

	public LoginPage() {
		userCredentials.put("requester.username","requester.password");
		userType.put("requester", userCredentials);
        userCredentials =  new HashMap<String,String>();
        userCredentials.put("serviceprovider.username","serviceprovider.password");
        userType.put("service provider", userCredentials);
        userCredentials =  new HashMap<String,String>();

	}
	
	/**
	 * Login to Center Point
	 * @param userRole
	 */
	public boolean loginCenterPoint(String userRole) {
		log.entry();
		boolean isPageLoaded = false;
		log.info("Logging in as [{}]", userRole);
		driverNavigator.get(PropertyUtil.getTestDataProp("centerpoint.url"));

		log.info(">>>" + userType.get(userRole.toLowerCase()));
		
		
		
		fld_Username.sendKeys(PropertyUtil.getTestDataProp("employee.username"));
		fld_Password.sendKeys(PropertyUtil.getTestDataProp("employee.password"));
		
		driverNavigator.clickButton(btn_LoginSubmit);
		return true;
	}

}
